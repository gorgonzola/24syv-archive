# Scraping af Radio 24syvs arkiv

Der lader til at være API til alt (gudskelov).

Liste over hele bagkataloget: <https://api.radio24syv.dk/v2/programs>
Metadata om alle udsendelser for et program: <https://api.radio24syv.dk/v2/podcasts/program/{videoProgramId}?size=10000&order=desc>
MP3 for udsendelse: <https://arkiv.radio24syv.dk/{item.audioInfo.url}>
