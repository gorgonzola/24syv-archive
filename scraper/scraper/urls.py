"""scraper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from programs import views as program_views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", program_views.start),
    path("programs/", program_views.ProgramsListView.as_view()),
    path("programs/<slug>/", program_views.ProgramDetailView.as_view()),
    path(
        "programs/<program_slug>/<int:broadcast_ext_id>/<broadcast_slug>/",
        program_views.broadcast_details,
    ),
    path("api/status", program_views.api_status),
    path("api/get_unsynced_broadcast", program_views.get_unsynced_broadcast),
    path("api/put_unsynced_broadcast", program_views.recv_unsynced_broadcast),
    path("api/get_asset_list/<slug:program_id>", program_views.get_asset_list),

    path("get_asset_list/<slug:program_id>", program_views.get_asset_list),

    
    path("asset_redirect/<int:ext_id>/<pretty_filename>", program_views.asset_redirect),

]
