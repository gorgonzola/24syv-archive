#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests


def get_programs():
    """Get data about all available programs."""
    return requests.get("https://api.radio24syv.dk/v2/programs").json()


def get_broadcasts_of_program(program):
    """Get data about all available broadcasts of a program."""
    broadcast_list = requests.get(
        "https://api.radio24syv.dk/v2/podcasts/program/{}?size=100&order=desc".format(
            program.ext_id
        )
    )
    if broadcast_list.status_code != 200:
        return []
    broadcast_list = broadcast_list.json()
    if len(broadcast_list) == 100:
        page_number = 2
        fetch_more = True
        while fetch_more is True:
            next_page = requests.get(
                "https://api.radio24syv.dk/v2/podcasts/program/{}?size=100&order=desc&p={}".format(
                    program.ext_id, page_number
                )
            )
            if next_page.status_code != 200:
                fetch_more = False
                return broadcast_list
            next_page = next_page.json()
            page_number += 1
            fetch_more = len(next_page) == 100
            broadcast_list += next_page

    return broadcast_list


"""
from tqdm import tqdm


errors = 0
total_size = 0
total_duration = 0
total_broadcasts = 0
catalog = requests.get("https://api.radio24syv.dk/v2/programs").json()
for program in tqdm(catalog):
    program_name = program.get("name")
    program_id = program.get("videoProgramId")
    if not program_id:
        tqdm.write("{} has no ID (skipping)".format(program_name))
        continue
    tqdm.write("Getting program list for {} ({})".format(program_name, program_id))
    program_list = requests.get(
        "https://api.radio24syv.dk/v2/podcasts/program/{}?size=100&order=desc".format(
            program_id
        )
    )
    if program_list.status_code != 200:
        tqdm.write(
            "Program list returned HTTP status {}".format(
                program_list.status_code
            )
        )
        errors += 1
        continue
    program_list = program_list.json()
    if len(program_list) == 100:
        page_number = 2
        fetch_more = True
        while fetch_more is True:
            tqdm.write("Getting page {}".format(page_number))
            next_page = requests.get(
                "https://api.radio24syv.dk/v2/podcasts/program/{}?size=100&order=desc&p={}".format(
                    program_id, page_number
                )
            )
            if next_page.status_code != 200:
                tqdm.write(
                    "Page {} returned HTTP status {}".format(
                        page_number, next_page.status_code
                    )
                )
                fetch_more = False
                errors += 1
                continue
            next_page = next_page.json()
            page_number += 1
            fetch_more = len(next_page) == 100
            program_list += next_page

    for broadcast in program_list:
        size = broadcast.get("audioInfo").get("fileSize")
        duration = broadcast.get("audioInfo").get("duration")
        if size is not None:
            total_size += size
        if duration is not None:
            total_duration += duration
    total_broadcasts += len(program_list)
    tqdm.write("{} broadcasts in {}".format(len(program_list), program_name))
tqdm.write("=" * 80)
tqdm.write(
    "{} programs, {} broadcasts, {} seconds, {} bytes".format(
        len(catalog), total_broadcasts, total_duration, total_size
    )
)
tqdm.write("{} errors".format(errors))
"""
