"""A management command to look for new broadcasts."""

from django.core.management.base import BaseCommand, CommandError

from programs import models


class Command(BaseCommand):
    """A management command to look for new broadcasts."""

    help = "Management command to look for new broadcasts." ""

    def handle(self, *args, **options):
        """Execute the command."""
        models.Program.update_all_broadcasts(only_active=False, only_new=True)
