"""A management command to download missing broadcast media."""

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from programs import models


class Command(BaseCommand):
    """A management command to download missing broadcast media."""

    help = "A management command to download all missing broadcast media."

    def handle(self, *args, **options):
        """Execute the command."""
        while models.Broadcast.objects.filter(
            download_status=models.Broadcast.DOWNLOAD_STATUS_NOT_DOWNLOADED
        ).count():
            b = models.Broadcast.objects.filter(
                download_status=models.Broadcast.DOWNLOAD_STATUS_NOT_DOWNLOADED
            ).first()
            self.stdout.write("{} {}".format(timezone.now(), b.program.name))
            self.stdout.write("{} {} {}".format(
                b.id, b.published_at, b.title
            ))
            b.download_audio()
        self.stdout.write("No more missing media to download.")
