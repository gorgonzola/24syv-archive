"""A management command to update the list of programs."""

from django.core.management.base import BaseCommand, CommandError

from programs import models


class Command(BaseCommand):
    """A management command to update the list of programs."""

    help = "Management command to update the list of programs."""

    def handle(self, *args, **options):
        """Execute the command."""
        models.Program.update_program_list()
