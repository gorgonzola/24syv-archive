"""A management command to download a single missing broadcast medium."""

import time

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from programs import models


class Command(BaseCommand):
    """A management command to download a single missing broadcast medium."""

    help = "Management command to download a single missing broadcast medium."

    def handle(self, *args, **options):
        """Execute the command."""
        if models.Broadcast.objects.filter(
            download_status=models.Broadcast.DOWNLOAD_STATUS_NOT_DOWNLOADED
        ).count():
            b = models.Broadcast.objects.filter(
                download_status=models.Broadcast.DOWNLOAD_STATUS_NOT_DOWNLOADED
            ).order_by("?").first()
            self.stdout.write("{} {}".format(timezone.now(), b.program.name))
            self.stdout.write("{} {} {}".format(
                b.id, b.published_at, b.title
            ))
            download_start_at = timezone.now()
            b.download_audio()
            download_duration = timezone.now() - download_start_at
            download_minutes = download_duration.seconds // 60
            download_seconds = download_duration.seconds % 60
            file_minutes = b.duration // 60
            file_seconds = b.duration % 60
            self.stdout.write("{}m {}s of mp3 downloaded in {}m {}s.".format(
                file_minutes,
                file_seconds,
                download_minutes,
                download_seconds,
            ))
        else:
            self.stdout.write("No missing media to download. Sleeping for a while, to avoid mayhem.")
            time.sleep(10)
