from django.shortcuts import redirect, render
from django.views.generic import DetailView, ListView
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

import os, json

from . import models
from . import scraper

from django.http import JsonResponse, HttpResponseRedirect, HttpResponseNotFound
from datetime import timedelta
from django.template.defaultfilters import slugify
from django.views.decorators.clickjacking import xframe_options_exempt


def start(request):
    """The start page."""
    media = models.Broadcast.objects.filter(
        download_status=models.Broadcast.DOWNLOAD_STATUS_DONE
    ).count()
    missing_media = models.Broadcast.objects.exclude(
        download_status=models.Broadcast.DOWNLOAD_STATUS_DONE
    ).count()
    broadcasts = media + missing_media
    media_pct = round(100.0 * media / broadcasts, 2) if broadcasts else 0
    missing_media_pct = (
        round(100.0 * missing_media / broadcasts, 2) if broadcasts else 0
    )
    return render(
        request,
        "programs/start.html",
        {
            "programs": models.Program.objects.count(),
            "broadcasts": broadcasts,
            "media": media,
            "media_pct": media_pct,
            "missing_media": missing_media,
            "missing_media_pct": missing_media_pct,
        },
    )


class ProgramsListView(ListView):
    """Listing of all programs."""

    queryset = models.Program.objects.order_by("name")
    context_object_name = "programs"
    template_name = "programs/programs.html"

    def no_post(self, request, *args, **kwargs):
        """Update the program listing."""
        programs = scraper.get_programs()
        for data in programs:
            ext_id = data.get("videoProgramId")
            if ext_id is not None:
                program = models.Program.objects.filter(ext_id=ext_id).first()
                if program is None:
                    program = models.Program()
                program.from_api_data(data)
                program.save()

        return redirect("/programs/")


class ProgramDetailView(DetailView):
    """A detail view for a program."""

    context_object_name = "program"
    queryset = models.Program.objects.all()
    template_name = "programs/details.html"

    def no_post(self, request, *args, **kwargs):
        """Update the broadcast listing of a program."""
        program = self.get_object()
        models.Broadcast.update_list_of_program(program)
        return redirect("/programs/{}/".format(program.slug))


def broadcast_details(request, program_slug, broadcast_ext_id, broadcast_slug):
    """A detail view for a broadcast."""
    broadcast = models.Broadcast.objects.get(ext_id=broadcast_ext_id)
    return render(
        request, "programs/broadcast-details.html", {"broadcast": broadcast}
    )


def download_speed():
    total = 0

    broadcasts = models.Broadcast.objects.filter(
        downloaded_at__gt=timezone.now() - timedelta(minutes=5)
    )

    for b in broadcasts:
        data = json.loads(b.raw_data)
        expected_size = data.get("audioInfo").get("fileSize")
        total += expected_size

    return total / (5 * 60)


def api_status(request):
    assets_downloaded = models.Broadcast.objects.filter(
        download_status=models.Broadcast.DOWNLOAD_STATUS_DONE
    ).count()
    assets_total = models.Broadcast.objects.count()

    return JsonResponse(
        {
            "assets_downloaded": assets_downloaded,
            "assets_total": assets_total,
            "bandwidth_in": download_speed(),
        }
    )


def get_unsynced_broadcast(request):
    # get first unsynced broadcast
    b = (
        models.Broadcast.objects.filter(
            download_status=models.Broadcast.DOWNLOAD_STATUS_NOT_DOWNLOADED
        )
        .order_by("?")
        .first()
    )

    if b is None:
        return JsonResponse({})
    
    p = b.program
    imageasset = None
    description = None
    try:
        p_data = json.loads(p.raw_data)
        imageasset = p_data["image"]["src"]
        description = p_data["description"]
    except:
        imageasset = ""
        description = ""

    return JsonResponse(
        {
            "ext_id": b.ext_id,
            "program": b.program.name,
            "title": b.title,
            "url": models.Broadcast.AUDIO_BASE_URL + b.audio_url,
            "imageasset": imageasset,
            "description": description,
        }
    )


@csrf_exempt
def recv_unsynced_broadcast(request):
    ext_id = request.POST.get("ext_id")
    broadcast = models.Broadcast.objects.filter(ext_id=ext_id).first()

    if broadcast is not None:
        # make sure the directory exists
        os.makedirs(broadcast.file_directory, exist_ok=True)

        with open(broadcast.file_path, "wb") as fp:
            for chunk in request.FILES["asset"].chunks():
                fp.write(chunk)

        # check the file size
        file_size = os.path.getsize(broadcast.file_path)
        broadcast_data = json.loads(broadcast.raw_data)

        if file_size == broadcast_data.get("audioInfo").get("fileSize"):
            broadcast.file_size_matches = models.Broadcast.SIZE_MATCH_YES
            broadcast.download_status = models.Broadcast.DOWNLOAD_STATUS_DONE
            broadcast.downloaded_at = timezone.now()
            broadcast.save()

            assets_downloaded = models.Broadcast.objects.filter(
                download_status=models.Broadcast.DOWNLOAD_STATUS_DONE
            ).count()
            assets_total = models.Broadcast.objects.count()

            return JsonResponse(
                {
                    "error": 0,
                    "status": "OK",
                    "assets_downloaded": assets_downloaded,
                    "assets_total": assets_total,
                }
            )
        else:
            broadcast.file_size_matches = models.Broadcast.SIZE_MATCH_NO
            broadcast.save()

            return JsonResponse({"error": 1, "status": "File size mismatch"})

    else:
        return JsonResponse({"error": 1, "status": "No file in POST"})


def asset_redirect(request, ext_id, pretty_filename):
    try:
        b = models.Broadcast.objects.get(ext_id=ext_id)
        program_id = b.program.slug
        
        b.redirect_counter += 1
        b.save()

        asset_url = models.Broadcast.AUDIO_BASE_URL + b.audio_url

        url_split = asset_url.split("/")
        url_split[-1] = "%s-%s-%s-%d.mp3" % (program_id, b.published_at.strftime("%y%m%d"), slugify(b.title), b.ext_id)

        asset_url = "/".join(url_split)
        
        return HttpResponseRedirect(asset_url)

    except models.Broadcast.DoesNotExist:
        return HttpResponseNotFound('<h1>Resource not found</h1>')


@xframe_options_exempt
def get_asset_list(request, program_id):

    fmt = request.GET.get('format', '')
    
    try:
        program = models.Program.objects.get(slug=program_id)
        broadcasts = program.broadcasts.order_by("published_at")

        broadcast_list = [
            {
                "published": b.published_at.strftime("%y-%m-%d"),
                "title": b.title,
                "url": "http://148.251.138.254/asset_redirect/%d/%s-%s-%s-%d.mp3" % (b.ext_id, program_id, b.published_at.strftime("%y%m%d"), slugify(b.title), b.ext_id)
            }
            for b in broadcasts ] 

        
        if fmt == "json":
            return JsonResponse(broadcast_list, safe=False)
        else:
            return render(request, 'assetlist.html', { "program": program, "broadcasts": broadcast_list } )

    except models.Program.DoesNotExist:
        return HttpResponseNotFound('<h1>Resource not found</h1>')
