# Generated by Django 2.2.4 on 2019-08-15 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='ext_id',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
    ]
