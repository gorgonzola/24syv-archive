# Generated by Django 2.2.4 on 2019-10-23 19:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0012_remove_broadcast_audio_is_downloaded'),
    ]

    operations = [
        migrations.AddField(
            model_name='broadcast',
            name='downlaoded_at',
            field=models.DateTimeField(default=None, null=True),
        ),
    ]
