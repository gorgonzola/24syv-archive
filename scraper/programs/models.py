import json
import os
import urllib

from django.db import models
from django.conf import settings
from django.utils import timezone
import requests

from . import scraper


class Program(models.Model):
    """A program is a series of related episodes."""

    raw_data = models.TextField()
    ext_id = models.PositiveIntegerField(unique=True)
    slug = models.CharField(max_length=500, null=True)
    name = models.CharField(max_length=250)
    last_broadcast_update_at = models.DateTimeField(null=True, default=None)
    is_active = models.BooleanField(default=True)

    def from_api_data(self, data):
        """Set object properties from raw API data."""
        self.raw_data = json.dumps(data)
        self.ext_id = data.get("videoProgramId")
        self.slug = data.get("slug")
        self.name = data.get("name")
        self.is_active = data.get("broadcastInfo").get("day") is not None

    @classmethod
    def update_all_broadcasts(cls, only_active=True, only_new=False):
        """
        Update broadcasts of all programs or only active programs.

        Additionally, it's possible to stop looking for new broadcasts when
        an existing broadcast is found.
        """
        # Prioritize active programs.
        programs = Program.objects.order_by("-is_active")
        # Update programs in list.
        for p in programs:
            if only_active and not p.is_active:
                print("Skipping inactive program {}.".format(p.name))
                continue
            print(timezone.now(), p.name, p.last_broadcast_update_at)
            Broadcast.update_list_of_program(p, only_new=only_new)

    @classmethod
    def update_program_list(cls):
        """Update list of programs."""
        print("Getting list of programs...")
        programs = requests.get("https://api.radio24syv.dk/v2/programs").json()
        for data in programs:
            ext_id = data.get("videoProgramId")
            if ext_id is not None:
                program = Program.objects.filter(ext_id=ext_id).first()
                if program is None:
                    print("Creating new program {}.".format(data.get("name")))
                    program = Program()
                else:
                    print(
                        "Updating existing program {}.".format(
                            data.get("name")
                        )
                    )
                program.from_api_data(data)
                program.save()


class Broadcast(models.Model):
    """A broadcast is a single episode in the series of a program."""

    AUDIO_BASE_URL = "https://arkiv.radio24syv.dk"

    SIZE_MATCH_UNKOWN = "SIZE_MATCH_UNKOWN"
    SIZE_MATCH_YES = "SIZE_MATCH_YES"
    SIZE_MATCH_NO = "SIZE_MATCH_NO"
    SIZE_MATCH_CHOICES = (
        (SIZE_MATCH_UNKOWN, "unknown"),
        (SIZE_MATCH_YES, "yes"),
        (SIZE_MATCH_NO, "no"),
    )

    DOWNLOAD_STATUS_NOT_DOWNLOADED = "DOWNLOAD_STATUS_NOT_DOWNLOADED"
    DOWNLOAD_STATUS_IN_PROGRESS = "DOWNLOAD_STATUS_IN_PROGRESS"
    DOWNLOAD_STATUS_DONE = "DOWNLOAD_STATUS_DONE"
    DOWNLOAD_STATUS_CHOICES = (
        (DOWNLOAD_STATUS_NOT_DOWNLOADED, "not downloaded"),
        (DOWNLOAD_STATUS_IN_PROGRESS, "in progress"),
        (DOWNLOAD_STATUS_DONE, "done"),
    )

    program = models.ForeignKey(
        Program, on_delete=models.CASCADE, related_name="broadcasts"
    )
    raw_data = models.TextField()
    ext_id = models.PositiveIntegerField(unique=True)
    published_at = models.DateTimeField(null=True)
    slug = models.CharField(max_length=500, null=True)
    title = models.CharField(max_length=500)
    duration = models.IntegerField()
    audio_url = models.CharField(max_length=500)
    download_status = models.CharField(
        max_length=50,
        choices=DOWNLOAD_STATUS_CHOICES,
        default=DOWNLOAD_STATUS_NOT_DOWNLOADED,
    )
    downloaded_at = models.DateTimeField(default=None, null=True)
    file_size_matches = models.CharField(
        max_length=30, choices=SIZE_MATCH_CHOICES, default=SIZE_MATCH_UNKOWN
    )
    redirect_counter = models.IntegerField(default=0)

    class Meta:
        """Meta class for the model."""

        ordering = ["-published_at"]

    @property
    def file_directory(self):
        """Get the directory path for the file."""
        return "/assets/{}".format(self.program.ext_id)

    @property
    def file_path(self):
        """Get the full path for the file."""
        return "{}/{}.mp3".format(self.file_directory, self.ext_id)

    def from_api_data(self, data):
        """Set object properties from raw API data."""
        self.raw_data = json.dumps(data)
        self.ext_id = data.get("videoPodcastId")
        self.slug = data.get("slug")
        self.title = data.get("title")
        self.duration = data.get("audioInfo").get("duration")
        self.audio_url = data.get("audioInfo").get("url")
        self.published_at = data.get("publishInfo").get("createdAt")

    def download_audio(self):
        """Download the audio file of a broadcast."""
        download_url = "{}{}".format(self.AUDIO_BASE_URL, self.audio_url)
        if not os.path.exists(self.file_directory):
            os.makedirs(self.file_directory)
        self.download_status = self.DOWNLOAD_STATUS_IN_PROGRESS
        self.save()
        with open(self.file_path, "wb") as f:
            mp3 = urllib.request.urlopen(download_url)
            f.write(mp3.read())
        self.download_status = self.DOWNLOAD_STATUS_DONE
        self.downloaded_at = timezone.now()
        self.file_size_matches = Broadcast.SIZE_MATCH_UNKOWN
        self.save()

    @classmethod
    def download_missing_media(cls):
        """Download media for broadcasts without them."""
        while Broadcast.objects.filter(
            download_status=cls.DOWNLOAD_STATUS_NOT_DOWNLOADED
        ).count():
            b = (
                Broadcast.objects.filter(
                    download_status=cls.DOWNLOAD_STATUS_NOT_DOWNLOADED
                )
                .order_by("?")
                .first()
            )
            print(b.id, b.published_at, b.program.name)
            print(b.title)
            b.download_audio()

    @classmethod
    def update_list_of_program(cls, program, page=1, only_new=False):
        """Update the list of broadcasts for the program."""
        BASE_URL = (
            "https://api.radio24syv.dk/v2/podcasts/program/"
            "{}?size=100&order=desc&p={}"
        )

        attempts = 0
        status = -1
        while status != 200:
            attempts += 1
            print("*" * 50)
            print(
                "Getting page {} for {} (#{}) (attempt {})...".format(
                    page, program.name, program.id, attempts
                )
            )
            broadcast_list = requests.get(
                BASE_URL.format(program.ext_id, page)
            )
            status = broadcast_list.status_code
            if attempts > 99:
                print(
                    (
                        "Giving up after 100 attempts for program={}, "
                        "page={}"
                    ).format(program.id, page)
                )
                return

        broadcast_list = broadcast_list.json()
        print(
            "Page {} contains {} broadcasts.".format(page, len(broadcast_list))
        )
        for data in broadcast_list:
            ext_id = data.get("videoPodcastId")
            if ext_id is not None:
                broadcast = Broadcast.objects.filter(ext_id=ext_id).first()
                if broadcast is None:
                    print("Saving new broadcast {}.".format(ext_id))
                    broadcast = Broadcast()
                else:
                    if only_new:
                        print("Done for now, as we only want new broadcasts.")
                        return
                    print("Updating existing broadcast {}.".format(ext_id))
                broadcast.from_api_data(data)
                broadcast.program = program
                broadcast.save()
        if len(broadcast_list) == 100:
            page += 1
            Broadcast.update_list_of_program(program, page)
        else:
            print(
                "Done after fetching {} pages of broadcasts for {}".format(
                    page, program.name
                )
            )
        program.last_broadcast_update_at = timezone.now()
        program.save()

    @classmethod
    def update_file_size_matches(cls):
        """Check whether file size matches for downloaded broadcasts."""
        while Broadcast.objects.filter(
            download_status=cls.DOWNLOAD_STATUS_DONE,
            file_size_matches=cls.SIZE_MATCH_UNKOWN,
        ).count():
            b = (
                Broadcast.objects.filter(
                    download_status=cls.DOWNLOAD_STATUS_DONE,
                    file_size_matches=cls.SIZE_MATCH_UNKOWN,
                )
                .order_by("?")
                .first()
            )
            # Handle potential race conditions happening between count() and
            # first() when running several instances of this.
            if not b:
                return
            data = json.loads(b.raw_data)
            expected_size = data.get("audioInfo").get("fileSize")
            try:
                actual_size = os.path.getsize(b.file_path)
                b.file_size_matches = (
                    cls.SIZE_MATCH_YES
                    if expected_size == actual_size
                    else cls.SIZE_MATCH_NO
                )
            # In case the files doesn't exist.
            except OSError:
                b.file_size_matches = cls.SIZE_MATCH_NO
            b.save()
